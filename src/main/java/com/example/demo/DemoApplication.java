package com.example.demo;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.web.bind.annotation.*;

@SpringBootApplication
@RestController
public class DemoApplication {

	@GetMapping("/")
	String home() {
		return "Spring is here!";
	}

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	public void hello(int a, int b, int c, int d, int e, int f)
	{
		System.out.println("too many variables!");
	}

	public int add(int a, int b, int c)
	{
		int sum = a + b;
		return sum;
	}

	public int sub(int a, int b)
	{
		int diff = a - b;
		return diff;
	}
}
